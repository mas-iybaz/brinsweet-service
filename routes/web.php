<?php
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Muhammad Iqbal Aulia';
});

$router->get('/data-statistic', 'StatisticController@index');

$router->get('/user', 'UserController@index');
$router->get('/user/{userId}', 'UserController@show');

$router->get('/product', 'ProductController@index');
$router->get('/product/{productId}', 'ProductController@show');
$router->post('/product', 'ProductController@store');
