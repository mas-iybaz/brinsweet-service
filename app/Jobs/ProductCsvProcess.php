<?php

namespace App\Jobs;
use App\Models\Product;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductCsvProcess extends Job implements ShouldQueue
{
    use Batchable, Queueable;

    public $data;
    public $header;
    public $created_by;
    public $created_by_desc;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $header, $created_by, $created_by_desc)
    {
        $this->data = $data;
        $this->header = $header;
        $this->created_by = $created_by;
        $this->created_by_desc = $created_by_desc;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->data as $product) {
            $productData = array_combine($this->header, $product);

            $productData['created_by'] = $this->created_by;
            $productData['created_by_desc'] = $this->created_by_desc;

            Product::create($productData);
        }
    }
}
