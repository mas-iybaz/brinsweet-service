<?php

namespace App\Http\Controllers;

use App\Models\ProductStorage;
use App\Models\Product;
use App\Jobs\ProductCsvProcess;
use Illuminate\Support\Facades\Bus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $products = Product::all();

        return response()->json([
            'status' => TRUE,
            'message' => 'All Product',
            'data' => $products
        ], 200);
    }

    public function show($productId)
    {
        $product = Product::find($productId);

        return response()->json([
            'status' => TRUE,
            'message' => 'Product',
            'data' => $product
        ], 200);
    }

    public function store(Request $request)
    {
        // Set Variable
        $core_path = "products";
        
        try {
            $this->validate($request, [
                'user_id' => 'required|exists:users,id',
                'user_name' => 'required|string|exists:users,name',
                'file' => 'required|file|mimetypes:text/csv'
            ]);

            DB::beginTransaction();

            $product_storage = new ProductStorage();
            
            // Upload CSV File
            $filename = time() . '_' . $request->input('user_id') . '.csv';
            $request->file('file')->move(storage_path($core_path), $filename);
            
            $product_storage->filepath = $core_path . '/' . $filename;
            $product_storage->created_by = $request->input('user_id');
            $product_storage->created_by_desc = $request->input('user_name');

            $product_storage->save();

            // Make a Job
            $batch = Bus::batch([])->dispatch();
            $data = file(storage_path($core_path) . '/' . $filename);
            $products = array_chunk($data, 100);
            $header = [];
            $product_created_by = $request->input('user_id');
            $product_created_by_desc = $request->input('user_name');

            foreach ($products as $key => $product) {
                $data = array_map('str_getcsv', $product);

                if ($key === 0) {
                    $header = $data[0];

                    unset($data[0]);
                }

                try {
                    $batch->add(new ProductCsvProcess($data, $header, $product_created_by, $product_created_by_desc));
                } catch (\Throwable $th) {
                    DB::rollBack();

                    return response()->json([
                        'status' => false,
                        'message' => 'Failed to insert data',
                        'data' => 'Wrong data format'
                    ], 400);        
                }
            }

            DB::commit();

            return response()->json([
                'status' => true,
                'message' => 'Successfully insert data',
                'data' => $product_storage
            ], 200);
        } catch (\Throwable $th) {
            DB::rollBack();

            return response()->json([
                'status' => false,
                'message' => 'Failed to insert data',
                'data' => $th->getMessage()
            ], 500);
        }
    }
}

