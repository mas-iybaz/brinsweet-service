<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Product;

class StatisticController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $total_users = User::all()->count();
        $total_products = Product::all()->count();

        $data = [
            'total_user' => $total_users,
            'total_product' => $total_products
        ];

        return response()->json([
            'status' => TRUE,
            'message' => 'Success',
            'data' => $data
        ], 200);
    }
}
