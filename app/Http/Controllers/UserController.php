<?php

namespace App\Http\Controllers;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $user = User::all();

        return response()->json([
            'status' => TRUE,
            'message' => 'Success',
            'data' => $user
        ], 200);
    }

    public function show($userId)
    {
        $user = User::find($userId);

        return response()->json([
            'status' => TRUE,
            'message' => 'Success',
            'data' => $user
        ], 200);
    }
}
